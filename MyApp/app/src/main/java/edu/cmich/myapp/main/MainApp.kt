package edu.cmich.myapp.main

import android.app.Application
import edu.cmich.myapp.models.PlacemarkMemStore
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class MainApp : Application(), AnkoLogger {

    val placemarks = PlacemarkMemStore()

    override fun onCreate() {
        super.onCreate()
        info("Placemark started")
    }
}